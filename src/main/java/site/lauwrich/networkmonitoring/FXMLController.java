package site.lauwrich.networkmonitoring;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.NetworkIF;

public class FXMLController implements Initializable {

    @FXML
    private Label speed, network;

    @FXML
    private ComboBox spinner;

    private NetworkIF[] nets;
    private NetModel myModel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nets = getNetworkIf();
        ArrayList<String> networks = new ArrayList<>();
        for (NetworkIF temp : nets) {
            networks.add(temp.getDisplayName());
        }
        
        ObservableList<String> networkObs = FXCollections.observableArrayList(networks);

        spinner.setItems(networkObs);

        spinner.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if (myModel != null) {
                    myModel.cleanup();
                }
                myModel = new NetModel(speed, getNetsFromName((String) newValue));
                network.setText((String) newValue);
            }
        });

    }

    private NetworkIF getNetsFromName(String name) {
        for (NetworkIF temp : nets) {
            if (name.equals(temp.getDisplayName())) {
                return temp;
            }
        }
        return null;
    }

    private NetworkIF[] getNetworkIf() {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        return hal.getNetworkIFs();
    }
    
    public void cleanup() {
        this.myModel.cleanup();
    }
}
