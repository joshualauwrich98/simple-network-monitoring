/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package site.lauwrich.networkmonitoring;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import oshi.hardware.NetworkIF;

/**
 *
 * @author DarKnight98
 */
public class NetModel implements Runnable {

    private Thread thread;
//    private Semaphore semaphore;
    private volatile boolean run;
    private Label speedLabel;
    private NetworkIF net;

    public NetModel(Label speedLabel, NetworkIF net) {
        this.thread = new Thread(this);
        this.speedLabel = speedLabel;
        this.net = net;
        this.run = true;
        this.thread.start();
    }

    public void cleanup() {
        try {
            this.run = false;
            this.thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(NetModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        long downloada, timestampa, downloadb, timestampb;
        
        while (true) {
            if (!run) break;
            try {
                downloada = net.getBytesRecv();
                timestampa = net.getTimeStamp();
                Thread.sleep(1000); //Sleep for a bit longer, 2s should cover almost every possible problem
                //                        net.updateNetworkStats();
                net.updateAttributes();
                downloadb = net.getBytesRecv();
                timestampb = net.getTimeStamp();
                final double finalSpeed = (downloadb - downloada) * 1.0 / (timestampb - timestampa) * 1.0;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (finalSpeed < 1)speedLabel.setText(String.format("%.3f %s", finalSpeed*1000, "bytes/s"));
                        else if (finalSpeed < 1000)speedLabel.setText(String.format("%.3f %s", finalSpeed, "KB/s"));
                        else if (finalSpeed < 1000000)speedLabel.setText(String.format("%.3f %s", finalSpeed/1000.0, "MB/s"));
                        else if (finalSpeed < 1000000000)speedLabel.setText(String.format("%.3f %s", finalSpeed/1000000.0, "GB/s"));
                        else speedLabel.setText(String.format("%.3f %s", finalSpeed/1000000000.0, "TB/s"));
                    }
                });
            } catch (InterruptedException ex) {
                Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (!run) break;
            }
        }
    }

}
